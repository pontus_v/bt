<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 22:14
 */
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
    'driver' => $app->config->get('database.driver'),
    'host' => $app->config->get('database.host'),
    'database' => $app->config->get('database.name'),
    'username' => $app->config->get('database.username'),
    'password' => $app->config->get('database.password'),
    'charset' => $app->config->get('database.charset'),
    'collation' => $app->config->get('database.collation'),
    'prefix' => $app->config->get('database.prefix')
]);

$capsule->bootEloquent();


