<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 21:03
 */

use Slim\Slim;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Noodlehaus\Config;

use Monolog\Logger;
use \Monolog\Handler\StreamHandler;

use Core\User\User;


session_cache_limiter(false);
session_start();

ini_set('display_errors', 'On');

define('INC_ROOT', dirname(__DIR__));

require INC_ROOT . '../vendor/autoload.php';

$app = new Slim([
    'mode' => file_get_contents(INC_ROOT . '/mode.php'),
    'view' => new Twig(),
    'templates.path' => INC_ROOT . '/app/views/'
]);

$app->configureMode($app->config('mode'), function() use ($app){
	$app->config = Config::load(INC_ROOT . "/app/config/{$app->mode}.php");
});

require 'database.php';
require 'controller.php';

$app->container->set('user', function(){
    return new User;
});


$app->container->singleton('log', function ($app) {
    $log = new Logger($app->config->get('site.name'));
    $log->pushHandler(new StreamHandler(INC_ROOT . '/app/logs/app.log', Logger::DEBUG));
    return $log;
});


// Setup the view
$view = $app->view();

$view->parserOptions = [
    'charset' => 'utf-8',
    'debug' => $app->config->get('twig.debug'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true,
    'cache' => INC_ROOT . '/app/views/cache'
];

$view->parserExtensions = [
  new TwigExtension()
];
//var_dump($app->user);